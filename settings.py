import pandas as pd
import numpy as np 
from utils import *

DATA_FOLDER = '/Users/renatosanabria/Desktop/data/demand_estimation/'
TARGET_COLS = ['city','state','clean_canonical_course_id','clean_canonical_course_name','course_kind','price_range']
TARGET_KINDS = [1,3]
PRICE_RANGE = ['popular','medium','premium']
SEMESTERS = "('2018.2','2019.1','2019.2')"
DEFAULT_PRICE = 10000
REDUCED_TARGET_COLS = list_remove(TARGET_COLS,'price_range') + ['family']
MIN_NUMBER_PRICE_POINTS = 15
PERC_REVENUE_THRESHOLD = .60
PROBABILITY_CUT = .3

