import pandas as pd
import numpy as np 
from utils import *
from settings import *

def aggregate_search_groups(df_search, df_spine):

	aux = df_spine.shape[0]

	def group_searches(group_col,group_name):
		return df_search.groupby(group_col)['id'].agg('count').reset_index().rename(columns={'id':group_name})
  
	#Searches by city
	group_col_dic = {
		'search_city':['city','state'],
		'search_city_kind': ['city','state','course_kind'],
		'search_state':['state'],
		'search_course': ['clean_canonical_course_id']  
	}
	for group_name,group_col in group_col_dic.items():
		df_spine = pd.merge(
			df_spine,
			group_searches(group_col,group_name),
			on = group_col,
			how='left'
		)
    
	assert df_spine.shape[0] == aux, "weird merge"
  
	return df_spine
