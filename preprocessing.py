import pandas as pd
import numpy as np 
from utils import *
from settings import *


def get_groupby_price_range(df_search,groupby_cols):
	"""
	Computes boundaries of price range (price_low,price_high) per group defined by groupby_cols
	"""
  
	df_range = df_search.groupby(groupby_cols)['maximum_value']\
					    .agg(lambda x: [np.nan,np.nan] if len(x) < MIN_NUMBER_PRICE_POINTS else [x.quantile(.3),x.quantile(.7)]) 
  
	df_range = pd.DataFrame(df_range.tolist(),columns=['price_low','price_high'],index=df_range.index)\
			     .reset_index()\
			     .sort_values(['price_low','price_high'],ascending=False)
  
	return df_range

def get_price_range(df_search):
	"""
	Determining price range per target (city,clean_canonical_course_id,course_kind)

	Groupby order:
	- FULL TARGET_COLS + family (city,state,course,course_kind, family)
	- FULL TARGET_COLS + family - course (city,state,course_kind,family)
	- FULL TARGET_COLS - course (city,state,course_kind) 
	- state,course_kind
	- course_kind
	"""	
	df_search_price = df_search[
		(df_search['maximum_value']!=DEFAULT_PRICE)&\
		(pd.isna(df_search['maximum_value'])==False)
	]
	
	#If a user searches for the same target in multiple days but with the same price, we drop it. 
	#But if she searches the same target in different days but with different prices, we keep (one user -> demand in multiple price ranges)
	df_search_price = df_search_price.drop_duplicates(REDUCED_TARGET_COLS + ['maximum_value','global_user_id'])

	#NOT FILLING NA
	# df_price_range = get_groupby_price_range(df_search_price,REDUCED_TARGET_COLS)

	#FILLING NA
	price_groups = [
		REDUCED_TARGET_COLS,
		list_remove(REDUCED_TARGET_COLS,['clean_canonical_course_id','clean_canonical_course_name']),
		list_remove(REDUCED_TARGET_COLS,['clean_canonical_course_id','clean_canonical_course_name','family']),
		list_remove(REDUCED_TARGET_COLS,['clean_canonical_course_id','clean_canonical_course_name','family','city']),
		list_remove(REDUCED_TARGET_COLS,['clean_canonical_course_id','clean_canonical_course_name','family','city','state']),
	]
  
	df_price_range = pd.DataFrame()
	for price_gr in price_groups:
		print("Grouping prices according to: ",price_gr)
		if df_price_range.empty:
			df_price_range = get_groupby_price_range(df_search_price,price_gr)
		else:
			aux = df_price_range.shape[0]
			df_price_range = pd.merge(
			  df_price_range,
			  get_groupby_price_range(df_search_price,price_gr),
			  on = price_gr,
			  how='left'
			)  
			assert df_price_range.shape[0] == aux, "merge failed!"
		
			df_price_range['price_low_x'] = df_price_range['price_low_x'].fillna(df_price_range['price_low_y'])    
			df_price_range['price_high_x'] = df_price_range['price_high_x'].fillna(df_price_range['price_high_y'])    
			df_price_range = df_price_range.drop(['price_low_y','price_high_y'],axis=1)\
										   .rename(columns={'price_low_x':'price_low','price_high_x':'price_high'})

		perc_fill = 1- float(pd.isna(df_price_range['price_low']).sum())/df_price_range.shape[0]
		print("% of targets filled: ", 100*perc_fill,"\n")
		if perc_fill == 1:
			break

	print("# of target ids from df_search_price: " ,df_price_range.shape[0])
	print("# of targets with price_range non-null" , df_price_range[pd.isna(df_price_range['price_low'])==False].shape[0] )
	df_price_range['clean_canonical_course_id'] = df_price_range['clean_canonical_course_id'].astype(int)
	
	return df_price_range



def binerize_price(df,df_price_range,price_col = 'maximum_value'):
	"""
	Method to binerize offered_price based on price_range
	df should contain TARGET_COLS-{price_range} and price_col 
	df_price_range should also contain TARGET_COLS-{price_range} and additional columns 'price_low', 'price_high'
	Currently binerizing in only 2 buckets
	"""
	assert set(df_price_range.columns.tolist()) == set(REDUCED_TARGET_COLS + ['price_low','price_high']),"Input df_price_range doesnt have the right columns "
	aux = df.shape[0]
	df = pd.merge(
		df,
		df_price_range,
		on=REDUCED_TARGET_COLS,
		how='left'
	)
	assert aux == df.shape[0],"weird merge"
	
	null_price_range = pd.isna(df['price_low']).sum()/df.shape[0] 
	if null_price_range>0:
		print("Warning!! ", 100*null_price_range," % of rows with missing price range")

		print("Warning!! Filling NA with overall average")
		df['price_low'] = df['price_low'].fillna(df['price_low'].mean())
		df['price_high'] = df['price_high'].fillna(df['price_high'].mean())

    
	df['price_range'] = (df[price_col]>df['price_low']).astype(int) + (df[price_col]>df['price_high']).astype(int)
	df['price_range'] = df['price_range'].map({0:PRICE_RANGE[0],1:PRICE_RANGE[1],2:PRICE_RANGE[2]})

	return df 




def aggregate_searches(df_search,agg_cols,perc_revenue):
  
    df_agg = df_search.groupby(agg_cols)['id'].agg('count').reset_index().sort_values('id',ascending=False).rename(columns={'id':'#searches'})
    df_agg['cum_perc_search'] = (df_agg['#searches'].cumsum())/df_agg['#searches'].sum()
    df_agg = df_agg.reset_index(drop=True).reset_index()
    if 'clean_canonical_course_id' in agg_cols:
        df_agg['clean_canonical_course_id']= df_agg['clean_canonical_course_id'].astype(int)
  
    top_agg = df_agg[df_agg['cum_perc_search']<=perc_revenue]#[agg_cols]
    top_agg = top_agg.rename(columns={'index':'rank'})
    top_agg['rank'] = top_agg['rank']+1
  
    return df_agg,top_agg[agg_cols + ['rank']]
