import pandas as pd
import numpy as np 


def cross_join(df1,df2):
  df1['_key'] = '_'
  df2['_key'] = '_'
  df_join = pd.merge(df1,df2,on='_key',how='left').drop('_key',axis=1)
  return df_join

def list_remove(ll,col):
  ll_copy = ll[:]
  if type(col)!=list:
    ll_copy.remove(col)
  else:
    for sub_col in col:
      ll_copy.remove(sub_col)
  return ll_copy

def evaluate_search_behavior(sdf_search, compute_perc = True):
  #Describing overall structure
  print("Describing")
  sdf_search_describe = sdf_search.describe()
  print("Topandas")
  df_pd = sdf_search_describe.toPandas()
  df_pd.T  
  
  if compute_perc:
    print("Warning!! Computing some percentages ... These takes some time ...")
    kind_presencial = sdf_search.where("array_contains(kinds,1)").where("! array_contains(kinds,3)").count()
    kind_ead = sdf_search.where("array_contains(kinds,3)").where("! array_contains(kinds,1)").count()
    kind_nonull = sdf_search.where(col('kinds').isNotNull()).count()
    kind_onlysemi = sdf_search.where("array_contains(kinds,8)").where(" ! array_contains(kinds,3)").where(" ! array_contains(kinds,1)").count()
    kind_semi = sdf_search.where("array_contains(kinds,8)").count()
  else:
    print("Warning! Using old values for perc of kind")
    kind_presencial = 4086870
    kind_ead = 1215493
    kind_nonull = 5308788
    kind_onlysemi = 5511
    kind_semi = 5198158


  aux = df_pd[df_pd['summary']=='count'].to_dict()
  print(" % of null cities: " ,100*(1-int(aux['city'][0])/int(aux['id'][0]))," %")
  print(" % of null search term: " ,100*(1-int(aux['search_term'][0])/int(aux['id'][0]))," %")
  print(" % of null canonical_course_id when search_term!=null: " , 100*(int(aux['search_term'][0])-int(aux['canonical_course_id'][0]))/int(aux['search_term'][0])," %")
  print(" % of rows with maximum_value==10000(default)", 100*(sdf_search.where(col('maximum_value') == 10000).count()/int(aux['id'][0])))
  print(" % of non-nulls kinds: ", 100*kind_nonull/int(aux['id'][0]))
  print(" % of kinds = Presencial: ", 100*kind_presencial/int(aux['id'][0]))
  print(" % of kinds = EaD: ", 100*kind_ead/int(aux['id'][0]))
  print(" % of kinds only  Semi: ", 100*kind_onlysemi/int(aux['id'][0]))
  print(" % of kinds contains  Semi: ", 100*kind_semi/int(aux['id'][0]))  

  return df_pd

def insert_dataframe(df,insert,column = None):
	'''
	This method inserts a dataframe (df1) with N rows into another dataframe
	with N rows (df2) - on axis 1 (columns) - in a specific position
	'''
	if column is None:
		column_idx = len(df.columns.tolist()) -1 #adding to the end of the df
	else:
		column_idx = df.columns.get_loc(column)
	df = pd.concat([df.iloc[:,:column_idx+1],insert,df.iloc[:,column_idx+1:]],axis=1)
	return df


################################################################
#						SPARK UDF
################################################################
# def unaccent_py(text):
#     if text is not None:
#         return unidecode(text)
#     else:
#         return None        
# _ =spark.udf.register("unaccent_py", unaccent_py, StringType())

