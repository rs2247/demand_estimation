import pandas as pd
import numpy as np 
from utils import *
from settings import *

import plotly.express as px
import plotly
import plotly.graph_objects as go


def opportunity_plot(df,hover_data,size,opportunity_col,export_file):

	# # aux = df_spine[df_spine['n_searches']<3000]
	# aux = df_spine[df_spine['n_searches']<np.inf]
	df['Modalidade'] = df['course_kind'].apply(lambda x: 'Presencial' if x==1 else 'EaD') 
	# df['opportunity'] = ((aux['total_revenue']==0)|(aux['n_av_offers'].round()==0)).astype(str)
	fig = px.scatter(
		df, 
		x="n_searches", 
		y="total_revenue", 
		color=opportunity_col,
		hover_data=hover_data,
		size=size,
		facet_row="Modalidade",
		category_orders={"Modalidade": ['Presencial','EaD'],opportunity_col:['False','True']}
	 )

	fig.update_layout(
		height=800,
		title_text="Distribuição de mercados (cidade/curso/modalidade/price_range) por #searches na home x total_revenue"
	)
	    
	if export_file:
		print("Exporting plot to file!")
		plotly.offline.plot(fig, filename='/Users/renatosanabria/Desktop/data/demand_estimation/plots/rev_search.html')

	return fig